package common;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Sasa on 5/14/2016.
 */
public class RMIInputStreamImpl implements RMIInputStreamInterf {

    private InputStream in;
    private byte[] b;

    public RMIInputStreamImpl(InputStream in) throws IOException {
        this.in = in;
        UnicastRemoteObject.exportObject(this, 1099);
    }

    public RMIPipe transfer(int key) throws IOException {
        return new RMIPipe(key, in);
    }

    public void close() throws IOException {
        in.close();
    }

    public int read() throws IOException {
        return in.read();
    }

    public byte[] readBytes(int len) throws IOException {
        if (b == null || b.length != len)
            b = new byte[len];

        int len2 = in.read(b);
        if (len2 < 0)
            return null; // EOF reached

        if (len2 != len) {
            // copy bytes to byte[] of correct length and return it
            byte[] b2 = new byte[len2];
            System.arraycopy(b, 0, b2, 0, len2);
            return b2;
        } else
            return b;
    }

}