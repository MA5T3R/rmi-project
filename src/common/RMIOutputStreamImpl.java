package common;

import java.io.IOException;
import java.io.OutputStream;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Sasa on 5/14/2016.
 */
public class RMIOutputStreamImpl implements RMIOutputStreamInterf {

    private OutputStream out;
    private RMIPipe pipe;

    public RMIOutputStreamImpl(OutputStream out) throws
            IOException {
        this.out = out;
        this.pipe = new RMIPipe(out);
        UnicastRemoteObject.exportObject(this, 1099);
    }

    public void write(int b) throws IOException {
        out.write(b);
    }

    public void write(byte[] b, int off, int len) throws
            IOException {
        out.write(b, off, len);
    }

    public void close() throws IOException {
        out.close();
    }

    public int getPipeKey() {
        return pipe.getKey();
    }

    public void transfer(RMIPipe pipe) throws IOException {
        // nothing more to do here
        // pipe has been serialized and that's all we want
    }

}
