package common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

/**
 * Created by Sasa on 5/14/2016.
 */
public class RMIOutputStream extends OutputStream implements
        Serializable {

    private int pipeKey;
    private RMIOutputStreamInterf out;

    public RMIOutputStream(RMIOutputStreamImpl out) {
        this.out = out;
    }

    public void write(int b) throws IOException {
        out.write(b);
    }

    public void write(byte[] b, int off, int len) throws
            IOException {
        out.write(b, off, len);
    }

    public void close() throws IOException {
        out.close();
    }


    public void transfer(InputStream in) throws IOException {
        RMIPipe pipe = new RMIPipe(pipeKey, in);
        out.transfer(pipe);
    }

}