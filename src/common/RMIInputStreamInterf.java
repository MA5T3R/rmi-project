package common;

import java.io.IOException;
import java.rmi.Remote;

/**
 * Created by Sasa on 5/14/2016.
 */
interface RMIInputStreamInterf extends Remote {

    byte[] readBytes(int len) throws IOException;

    int read() throws IOException;

    void close() throws IOException;

    RMIPipe transfer(int pipe) throws IOException;
}
