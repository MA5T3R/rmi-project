package common;

import java.io.IOException;
import java.rmi.Remote;

/**
 * Created by Sasa on 5/14/2016.
 */
interface RMIOutputStreamInterf extends Remote {

    void write(int b) throws IOException;

    void write(byte[] b, int off, int len) throws
            IOException;

    void close() throws IOException;

    void transfer(RMIPipe pipe) throws IOException;
}