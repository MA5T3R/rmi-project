package client;

import common.PropertyValues;
import common.RMIInputStream;
import common.RMIOutputStream;
import server.ServerInterf;

import java.io.*;
import java.rmi.Naming;
import java.util.Map;

/**
 * Created by Sasa on 5/14/2016.
 */
public class DriverCl {

    final private static int BUF_SIZE = 1024 * 64;

    private static void copy(InputStream in, OutputStream out)
            throws IOException {

        if (in instanceof RMIInputStream) {
            //System.out.println("using RMIPipe of RMIInputStream");
            ((RMIInputStream) in).transfer(out);
            return;
        }

        if (out instanceof RMIOutputStream) {
            //System.out.println("using RMIPipe of RMIOutputStream");
            ((RMIOutputStream) out).transfer(in);
            return;
        }

        //System.out.println("using byte[] read/write");
        byte[] b = new byte[BUF_SIZE];
        int len;
        while ((len = in.read(b)) >= 0) {
            out.write(b, 0, len);
        }
        in.close();
        out.close();
    }

    private static void upload(ServerInterf server, File src,
                               File dest) throws IOException {
        copy(new FileInputStream(src),
                server.getOutputStream(dest));
    }

    private static void download(ServerInterf server, File src,
                                 File dest) throws IOException {
        copy(server.getInputStream(src),
                new FileOutputStream(dest));
    }

    public static void main(String[] args) throws Exception {

        Map properties = new PropertyValues().getPropValues();
        ServerInterf server = (ServerInterf) Naming.lookup((String) properties.get("url"));

        System.out.println("Server says: " + server.sayHello());

        //parameters to keep track of how many commands were executed
        int command_count = 0;

        //current path
        String location = server.definePath();
        System.out.println("Working directory: " + location);

        String location_cur = location;

        //read from input
        System.out.println("\nIntroduceti comanda dorita:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String command = br.readLine();
        String[] input = command.split(" ");

        while (!command.equals("exit")) {

            if (input.length == 1) {
                //dir
                if (command.equals("dir")) {
                    System.out.println("Rezultatul comenzii este \n" + server.retrieveFoldersAndFiles(location_cur));
                } else
                    System.out.println("\nComanda introdusa e gresita");


            } else if (input.length == 2) {
                //ptr put, get si cd
                if (input[0].equals("cd")) {
                    if (input[1].equals("..")) {
                        if (command_count == 0) {
                            System.out.println("Comanda nu poate fi efectuata. Directorul curent este radacina.");
                        } else {
                            //daca suntem in working directory nu putem executa comanda 'cd ..'
                            if (location_cur.equals(location)) {
                                System.out.println("Comanda nu poate fi efectuata. Directorul curent este radacina.");
                            } else {
                                String[] loc = location_cur.split("\\\\");
                                String str = "";
                                for (int i = 0; i < loc.length - 1; i++) {
                                    if (str.equals(""))
                                        str = str + loc[i];
                                    else
                                        str = str + "\\" + loc[i];
                                }
                                location_cur = str;
                                command_count++;
                            }
                        }

                    } else {

                        File[] files = server.retrieveFileClass(location_cur);
                        boolean ok = false;
                        for (File file : files) {
                            if (file.getName().equals(input[1])) {
                                ok = true;
                            }
                        }
                        if (!ok) {
                            System.out.println("Parametrul dat comenzii este eronat.");
                        } else {
                            System.out.println("Comanda a fost efectuata cu succes");
                            location_cur = location_cur + "\\" + input[1];
                            command_count++;
                        }
                    }
                }
                if (input[0].equals("get")) {
                    try {
                        File[] files = server.retrieveFileClass(location_cur);
                        for (File file : files) {
                            if (file.getName().equals(input[1])) {
                                download(server, file, new File(file.getName()));
                            }
                        }
                        System.out.println("Comanda a fost efectuata cu succes");
                    } catch (Exception e) {
                        System.out.println("\nFisierul sursa nu exista.");
                    }
                }

                if (input[0].equals("put")) {
                    try {
                        System.out.println(System.getProperty("user.dir") + "\\" + input[1]);
                        System.out.println(location_cur);
                        upload(server, new File(System.getProperty("user.dir") + "\\" + input[1]), server.getCurrentFile(location_cur + "\\" + input[1]));
                    } catch (Exception e) {
                        System.out.println("\nFisierul sursa nu exista.");
                    }
                    System.out.println("Comanda a fost efectuata cu succes");
                }

            } else
                System.out.println("\nComanda introdusa e gresita");

            System.out.println("\nIntroduceti comanda dorita:");
            br = new BufferedReader(new InputStreamReader(System.in));
            command = br.readLine();
            input = command.split(" ");
        }

        /*

        File testFile = new File("Test100MB.tif");
        float len = testFile.length();

        float t;
        t = System.currentTimeMillis();
        download(server, testFile, new File("download.tif"));
        t = (System.currentTimeMillis() - t) / 1000;
        if (t != 0)
            System.out.println("download: " + (len / t / 1000000) + " MB/s");
        else
            System.out.println("download: too fast :O");

        t = System.currentTimeMillis();
        upload(server, new File("download.tif"), new File("upload.tif"));
        t = (System.currentTimeMillis() - t) / 1000;
        if (t != 0)
            System.out.println("upload: " + (len / t / 1000000) +
                    " MB/s");
        else
            System.out.println("upload: too fast :O");

    */
    }

}
