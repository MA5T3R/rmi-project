package server;

/**
 * Created by Sasa on 5/14/2016.
 */
public class DriverSV {

    public static void main(String[] args) throws Exception {
        ServerImpl server = new ServerImpl();
        server.start();
        Thread.sleep(10 * 60 * 1000); // run for 10 minutes
        server.stop();
    }

}
