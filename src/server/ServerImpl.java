package server;

import common.RMIInputStream;
import common.RMIInputStreamImpl;
import common.RMIOutputStream;
import common.RMIOutputStreamImpl;

import java.io.*;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Sasa on 5/15/2016.
 */
public class ServerImpl extends UnicastRemoteObject
        implements ServerInterf {

    Registry rmiRegistry;

    ServerImpl() throws RemoteException {
        super();
    }

    void start() throws Exception {
        rmiRegistry = LocateRegistry.createRegistry(1099);
        rmiRegistry.bind("server", this);
        System.out.println("Server started");
    }

    void stop() throws Exception {
        rmiRegistry.unbind("server");
        unexportObject(this, true);
        unexportObject(rmiRegistry, true);
        System.out.println("Server stopped");
    }

    public String sayHello() {
        return "Hello world";
    }

    public OutputStream getOutputStream(File f) throws IOException {
        return new RMIOutputStream(new RMIOutputStreamImpl(new
                FileOutputStream(f)));
    }

    public InputStream getInputStream(File f) throws IOException {
        return new RMIInputStream(new RMIInputStreamImpl(new
                FileInputStream(f)));
    }

    public String definePath() throws IOException {
        return "" + System.getProperty("user.dir");
    }

    //dir command din working directory
    public String retrieveFoldersAndFiles(String location) throws IOException {
        File f = new File(location); // current directory
        String filesAndFolders = "";
        File[] files = f.listFiles();

        for (File file : files != null ? files : new File[0]) {
            if (file.isDirectory()) {
                filesAndFolders = filesAndFolders + "directory:";
            } else {
                filesAndFolders = filesAndFolders + "     file:";
            }
            filesAndFolders = filesAndFolders + file.getCanonicalPath() + "\n";
        }
        return filesAndFolders;
    }

    public File getCurrentFile(String location) throws IOException {
        return new File(location);
    }

    public File[] retrieveFileClass(String location_cur) throws IOException {
        return new File(location_cur).listFiles();
    }

}