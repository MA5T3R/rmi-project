package server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Sasa on 5/14/2016.
 */
public interface ServerInterf extends Remote {
    String sayHello() throws RemoteException;

    OutputStream getOutputStream(File f) throws IOException;

    InputStream getInputStream(File f) throws IOException;

    String definePath() throws IOException;

    String retrieveFoldersAndFiles(String location) throws IOException;

    File getCurrentFile(String location) throws IOException;

    File[] retrieveFileClass(String location) throws IOException;

}
